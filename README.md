Please check the *Chess.pdf* file to see the final board.

Package `ctex` is used to typeset Chinese characters.

Additional fonts may be required to successfully compile the file, including simhei, kaiti and lisu.